import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { SearchType } from 'src/app/enums/search-type';
import { SearchResponse } from 'src/app/interfaces/search-response';
import { SearchService } from 'src/app/services/search.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
})
export class SearchComponent implements OnInit {
  searchResponse?: SearchResponse;

  constructor(
    private route: ActivatedRoute,
    private searchService: SearchService
  ) {}

  /**
   * @brief Loads the search results using parameters from the URL.
   *
   * @details Reads the quiery parameters from the URL. Calls the search service with the parameters and sets callback
   * functions to show the results to the user when it arrives.
   */
  ngOnInit(): void {
    this.route.queryParams.subscribe((params: any) => {
      let observable: Observable<SearchResponse> | undefined;
      if (params.title) {
        observable = this.searchService.searchBy(
          SearchType.Title,
          params.title
        );
      } else if (params.author) {
        observable = this.searchService.searchBy(
          SearchType.Author,
          params.author
        );
      }
      observable?.subscribe((searchResponse: SearchResponse) => {
        this.searchResponse = searchResponse;
      });
    });
  }
}
