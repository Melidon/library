import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Work, Description } from 'src/app/interfaces/work';
import { WorkService } from 'src/app/services/work.service';

@Component({
  selector: 'app-works',
  templateUrl: './works.component.html',
  styleUrls: ['./works.component.css'],
})
export class WorksComponent implements OnInit {
  work?: Work;

  constructor(private route: ActivatedRoute, private service: WorkService) {}

  /**
   * @brief Loads a work using parameters from the URL.
   *
   * @details Reads work key from the URL. Calls the work service with the key and sets callback function to show the
   * result to the user when it arrives.
   */
  ngOnInit(): void {
    this.route.paramMap.subscribe((param: any) => {
      if (param.params.key) {
        this.service.getWork(param.params.key).subscribe((work: Work) => {
          this.work = work;
        });
      }
    });
  }

  /**
   * @brief Returns the description of the current work.
   *
   * @details The description of a Work may be a string or a Details object that has a string value. This function is to
   * decide which one to use.
   *
   * @return string The description of a work.
   */
  description(): string | undefined {
    if (this.work === undefined) {
      return undefined;
    }
    if (typeof this.work.description === 'string') {
      return this.work.description;
    } else {
      return (this.work.description as Description).value;
    }
  }
}
