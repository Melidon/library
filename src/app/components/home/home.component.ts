import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { SearchType } from 'src/app/enums/search-type';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent {
  searchTypes: SearchType[] = Object.values(SearchType);

  term: string = '';
  searchType: SearchType = SearchType.Title;

  constructor(private router: Router) {}

  /**
   * @brief Searches based on the user input.
   *
   * @details Redirects to the search page while passing the search arguments in the URL.
   */
  search(): void {
    if (this.term.trim()) {
      let queryString = this.term.split(' ').join('+');
      let object: any;
      switch (this.searchType) {
        case SearchType.Title:
          object = { title: queryString };
          break;
        case SearchType.Author:
          object = { author: queryString };
          break;
      }
      this.router.navigate(['/search'], { queryParams: object });
    }
  }
}
