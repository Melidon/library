export interface Work {
    description?:        Description | string;
    links?:              Link[];
    title:               string;
    covers?:             number[];
    subject_places?:     string[];
    first_publish_date?: string;
    subject_people?:     string[];
    key:                 string;
    authors?:            Author[];
    excerpts?:           Excerpt[];
    subjects?:           string[];
    type:                Type;
    subject_times?:      string[];
    latest_revision:     number;
    revision:            number;
    created:             Created;
    last_modified:       Created;
    id?:                 number;
    subtitle?:           string;
    dewey_number?:       string[];
    lc_classifications?: string[];
}

export interface Author {
    author: Type;
    type:   Type;
}

export interface Type {
    key: string;
}

export interface Description {
    type:  Type;
    value: string;
}

export interface Created {
    type:  Type;
    value: Date;
}

export interface Excerpt {
    pages?:   string;
    excerpt:  string;
    author:   Type;
    comment?: string;
}

export interface Link {
    title: string;
    url:   string;
    type:  Type;
}
