import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable, throwError } from 'rxjs';
import { Work } from '../interfaces/work';

@Injectable({
  providedIn: 'root',
})
export class WorkService {
  constructor(private httpClient: HttpClient) {}

  /**
   * @brief Returns an Observable<Work> based on the given key.
   *
   * @details If an error occures it shows an alert dialog with the error message and rethrows the error.
   *
   * @param key The key of the Work to find.
   * @return Observable<Work>
   */
  getWork(key: string): Observable<Work> {
    return this.httpClient
      .get<Work>(`https://openlibrary.org/works/${key}.json`)
      .pipe(
        catchError((error) => {
          alert(error.message);
          return throwError(() => error);
        })
      );
  }
}
