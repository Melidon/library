import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable, throwError } from 'rxjs';
import { SearchType } from '../enums/search-type';
import { SearchResponse } from '../interfaces/search-response';

@Injectable({
  providedIn: 'root',
})
export class SearchService {
  private static baseURL: string = 'https://openlibrary.org/search.json?';

  constructor(private httpClient: HttpClient) {}

  /**
   * @brief Searches a book by a search type and a term.
   *
   * @details If an error occures it shows an alert dialog with the error message and rethrows the error.
   *
   * @param searchType The type of the search, like title or author.
   * @param term The search term.
   * @return Observable<SearchResponse>
   */
  searchBy(searchType: SearchType, term: string): Observable<SearchResponse> {
    let queryString: string = SearchService.baseURL;
    switch (searchType) {
      case SearchType.Title:
        queryString += `title=${term}`;
        break;
      case SearchType.Author:
        queryString += `author=${term}`;
        break;
    }
    return this.httpClient.get<SearchResponse>(queryString).pipe(
      catchError((error) => {
        alert(error.message);
        return throwError(() => error);
      })
    );
  }
}
